#include "libft.h"

void	ft_arrfree(char **tab)
{
	char	**beg;

	beg = tab;
	if (!tab)
		return ;
	while (*tab)
		free(*tab++);
	free(beg);
	beg = NULL;
}
