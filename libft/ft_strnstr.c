/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nw_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 17:31:55 by hskikdi           #+#    #+#             */
/*   Updated: 2019/11/07 16:32:37 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	needle_size;
	size_t	i;

	if (!*little)
		return ((char*)(big));
	needle_size = ft_strlen(little);
	i = 0;
	while (*(big + i) && i + needle_size <= len)
	{
		if (!ft_strncmp(big + i, little, needle_size))
			return ((char*)big + i);
		i++;
	}
	return (NULL);
}
