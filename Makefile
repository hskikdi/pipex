
OBJ=$(addsuffix .o, src/utils src/pipex)

NAME=pipex

FLAG=-Wall -Werror -Wextra

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft
	gcc $(FLAG) $^ -o $@ -Llibft -lft

%.o: %.c
	gcc $(FLAG) -c -o $@ $< -Iincludes

clean:
	make clean -C libft
	rm -rf $(OBJ)

fclean: clean
	rm -f $(NAME)
	make fclean -C libft

re: fclean all
