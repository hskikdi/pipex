#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "libft/libft.h"

char	*get_env(char *s, char **tab);

void	print_env(char **tab)
{
	int i = 0;
	while (tab[i])
		printf("%s\n", tab[i++]);
}

int		ft_error(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	write(2, str, len);
	write(2, "\n", 1);
	exit(0);
}

int		ft_open_file(char *file, int mode)
{
	int		fd;

	if (!mode)
		fd = open(file, O_RDONLY);
	else
		fd = open(file, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG);
	return (fd);
}

char	*create_path(char *s1, char *s2)
{
	char	*path;
	char	*tmp;

	if (!(tmp = ft_strjoin(s1, "/")))
		ft_error("malloc error");
	if (!(path = ft_strjoin(tmp, s2)))
		ft_error("malloc error");
	ft_strdel(&tmp);
	return (path);
}

char		*find_exec(char *name, char **env)
{
	char	**paths;
	char	*tmp;
	char	**to_free;

	tmp = get_env("PATH", env);
//	printf("tmp = %s\n", tmp);
	paths = ft_split(tmp, ':');
	to_free = paths;
	ft_strdel(&tmp);
//	printf("tmp = %s\n", tmp);
	while (*paths)
	{
		tmp = create_path(*paths, name);
		if (tmp && access(tmp, F_OK) != -1 && access(tmp, R_OK) != -1)
		{
			ft_arrfree(to_free);
			printf("tmp = %s\n", tmp);
			return (tmp);
		}
		ft_strdel(&tmp);
		paths++;
	}
	ft_arrfree(to_free);
	return (ft_strdup(""));
}

void	ft_exec_child(char *infile, char *cmd, char **env, int *fd)
{
	int		fd_infile;
	char	**params;
	char	*exec;


	fd_infile = ft_open_file(infile, 0); //close avant de dup?
	if (fd_infile == -1)
		ft_error("open probleme infile");	//changer le message dèrreur
	dup2(fd_infile, STDIN_FILENO);
	dup2(fd[1], STDOUT_FILENO);
	params = ft_split(cmd, ' ');//a proteger
	exec = find_exec(*params, env);
	execve(exec, params, env);
	ft_putstr_fd("pipex commande not found: ", 2); //leasks?
	ft_error(*params);
}

void	ft_exec_father(char *cmd, char *outfile, char **env, int *fd)
{
	int		fd_outfile;
	char	**params;
	char	*exec;

	wait(0);
	fd_outfile = ft_open_file(outfile, 1); //close avant de dup?
	if (fd_outfile == -1)
		ft_error("open probleme infile"); //changer le message dèrreur
	close(fd[1]);
	dup2(fd[0], STDIN_FILENO);
	close(fd[0]);
	dup2(fd_outfile, STDOUT_FILENO);
	params = ft_split(cmd, ' ');//a proteger
	exec = find_exec(*params, env);
	execve(exec, params, env);
	ft_putstr_fd("pipex commande not found: ", 2); //leasks?
	ft_error(*params);
}

int		main(int ac, char **av, char **env)
{
	int		child;
	int		fd[2];

	if (ac != 5)
		ft_error("usage pipex : 4 arguments expected");
	if (pipe(fd) == -1)
		ft_error("pipe error");
	child = fork();
	if (child < 0)
		ft_error("fork error");
	if (!child)
	{
		ft_exec_child(av[1], av[2], env, fd);
	}
	else
	{
		ft_exec_father(av[3], av[4], env, fd);
	}
	return (0);
}
